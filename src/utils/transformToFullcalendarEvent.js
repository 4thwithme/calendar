import getRecurringDates from './getRecurringDates';

export default (event, oldEvent) => {
  const start = new Date(
    typeof oldEvent === 'object' ? oldEvent.start : event.start
  );
  const end = new Date(typeof oldEvent === 'object' ? oldEvent.end : event.end);

  if (event.rrule) {
    const recurringEvents = getRecurringDates(
      event.rrule.interval,
      event.rrule.byweekday,
      event.rrule.dtstart,
      event.rrule.until
    );

    const duration = event.rrule.duration || 1;

    return recurringEvents.map(dateStr => {
      const date = new Date(dateStr);

      return {
        allDay: true,
        id: event._id,
        title: event.title,
        start: date,
        end: new Date(date.getTime() + 24 * 60 * 60 * 1000 * duration),
        initialStartDay: date.getDay(),
        duration,
      };
    });
  }

  return {
    allDay: true,
    id: event._id,
    title: event.title,
    start,
    end,
    initialStartDay: new Date(start).getDay(),
    duration: event.rrule ? event.rrule.duration : 1,
  };
};
