import moment from 'moment';
require('moment-recur');

// interval: 1 | 2 | 3 | 4; <- weeks interval
// days:  0 | 1 | 2 | 3 | 4 | 5 | 6  or Array of this numbers;
// startDate: 09/12/2012
// endDate: 09/12/2019

export default (interval, days, startDate, endDate) => {
  const start = new Date(startDate);
  const end = new Date(endDate);

  const weeksOfMonth = [];

  for (let i = 0; i < 5; i += interval) {
    weeksOfMonth.push(i);
  }

  const recurDatesArr = moment()
    .recur({ start, end })
    .every(days)
    .daysOfWeek()
    .every(weeksOfMonth)
    .weeksOfMonthByDay();

  return recurDatesArr.all().map(mom => {
    return mom.utc().format();
  });
};
