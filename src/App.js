import React from 'react';
import { hot } from 'react-hot-loader';
import moment from 'moment';
import FullCalendar from 'fullcalendar-reactwrapper';
import Modal from 'react-responsive-modal';
import DatePicker from 'react-datepicker';
import Select from 'react-select';

import { transformToFullcalendarEvent } from './utils';
import API from './api/api';

import EventDetailsForm from './EventDetailsForm';
import { options, intervalOtpions } from './constants';

// See the FullCalendar API for details on how to use it:
// https://fullcalendar.io/docs
// We're using fullcalendar-reactwrapper to wrap it in a React
// component.

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      events: [],
      modalOpen: false,
      newEvent: false,
      openedEventTitle: '',
      openedEventId: '',
      isRecurMode: false,
      rrule: {
        freq: 'weekly',
        interval: intervalOtpions[0],
        byweekday: [options[0]],
        dtstart: new Date(),
        until: new Date(new Date().getTime() + 30 * 24 * 60 * 60 * 1000),
      },
    };
  }

  componentDidMount() {
    this.fetchAndLoadEvents();
  }

  updateEventDates = e => {
    // start = midnight on day of in UTC
    // end = start + 24 hours - 1 second
    const momentStart = moment(e.start.toDate());
    let momentEnd = moment(e.start.toDate())
      .add(24, 'hours')
      .add(-1, 'seconds');
    // Use event.end if it's present. 1 day events
    // do not have event.end if they're coming from Fullcalendar.
    if (e.end) {
      momentEnd = e.end;
    }

    API.updateEventDate(e.id, momentStart, momentEnd, e.initialStartDay)
      .then(({ data }) => {
        const { events } = this.state;

        const filteredEvents = events.filter(event => event.id !== data._id);

        const updatedEvents = data.rrule
          ? [...filteredEvents, ...transformToFullcalendarEvent(data)]
          : [...filteredEvents, transformToFullcalendarEvent(data)];

        this.setState({
          events: updatedEvents,
        });
      })
      .catch(console.error);
  };

  onEventDrop = e => {
    this.updateEventDates(e);
  };

  onEventResize = e => {
    this.updateEventDates(e);
  };

  onEventClick = e => {
    this.setState({
      modalOpen: true,
      openedEventTitle: e.title,
      openedEventId: e.id,
    });
  };

  onCloseModal = () => {
    // Update the event
    const { openedEventId, openedEventTitle, newEvent, rrule } = this.state;

    if (!openedEventTitle.trim().length) {
      return this.setState({ modalOpen: false });
    }

    let updatedRrule;

    if (rrule.isRecur) {
      updatedRrule = {
        ...rrule,
        interval: rrule.interval.value,
        byweekday: rrule.byweekday.map(d => d.value),
      };
    }

    if (newEvent) {
      API.addNewEvent(newEvent, openedEventTitle, updatedRrule)
        .then(({ data }) => {
          const { events } = this.state;

          events.push(transformToFullcalendarEvent(data));
          this.setState({ events: events.flat() });
        })
        .catch(console.error);
    } else {
      API.updateEventTitle(openedEventId, openedEventTitle, updatedRrule)
        .then(({ data: updatedEvent }) => {
          delete updatedEvent.rrule;
          // Find the event in our events array and update it accordingly
          const { events } = this.state;

          const updatedEvents = events.map(_event => {
            if (_event.id === updatedEvent._id) {
              return transformToFullcalendarEvent(updatedEvent, _event);
            }
            return _event;
          });
          this.setState({ events: updatedEvents });
        })
        .catch(console.error);
    }
    // Close modal
    this.setState({ modalOpen: false });
  };

  onExitedModal = () => {
    // Reset state on animation end? Who put this here??
    this.setState({
      openedEventId: '',
      openedEventTitle: '',
      newEvent: false,
      isRecurMode: false,
    });
  };

  onTitleChange = event => {
    this.setState({ openedEventTitle: event.currentTarget.value });
  };

  onDelete = () => {
    const { openedEventId } = this.state;

    API.deleteEvent(openedEventId).then(() => {
      this.fetchAndLoadEvents();
    });
    // Close modal
    this.setState({
      modalOpen: false,
    });
  };

  onDayClick = clickedDate => {
    // clicked date = midnight on day of in UTC
    // start = clicked date
    // end = start + 24 hours - 1 second
    const momentStart = moment(clickedDate.toDate());
    const momentEnd = moment(clickedDate.toDate())
      .add(24, 'hours')
      .add(-1, 'seconds');

    const newEvent = {
      title: 'New event',
      start: momentStart.toDate(),
      end: momentEnd.toDate(),
    };

    this.setState({
      newEvent,
      modalOpen: true,
    });
  };

  handleSelectOnChange = selectedOption =>
    this.setState({
      rrule: {
        ...this.state.rrule,
        byweekday: selectedOption,
      },
    });

  toggleCheckbox = () =>
    this.setState({
      isRecurMode: !this.state.isRecurMode,
      rrule: {
        ...this.state.rrule,
        isRecur: !this.state.isRecurMode,
      },
    });

  handleStartOnChange = date =>
    this.setState({
      rrule: {
        ...this.state.rrule,
        dtstart: date,
      },
    });

  handleIntervalOnChange = selectedOption =>
    this.setState({
      rrule: {
        ...this.state.rrule,
        interval: selectedOption,
      },
    });

  handleEndOnChange = date =>
    this.setState({
      rrule: {
        ...this.state.rrule,
        until: date,
      },
    });

  fetchAndLoadEvents = () =>
    API.fetchAllEvents()
      .then(({ data }) => {
        const newEvents = data.map(transformToFullcalendarEvent);

        this.setState({ events: newEvents.flat() });
      })
      .catch(console.error);

  render() {
    const {
      events,
      modalOpen,
      openedEventTitle,
      newEvent,
      isRecurMode,
      rrule,
    } = this.state;

    console.log(this.state);

    return (
      <div>
        <FullCalendar
          editable={true}
          timezone="UTC"
          events={events}
          eventDrop={this.onEventDrop}
          eventResize={this.onEventResize}
          eventClick={this.onEventClick}
          dayClick={this.onDayClick}
        />
        <Modal
          open={modalOpen}
          onClose={this.onCloseModal}
          onExited={this.onExitedModal}
          center
          closeIconSize={14}
          classNames={{ modal: 'event-modal', closeIcon: 'modal-close' }}
        >
          <h5>{newEvent ? 'New Event' : 'Edit Event'}</h5>

          <EventDetailsForm
            value={openedEventTitle}
            showDelete={!newEvent}
            onChange={this.onTitleChange}
            onDelete={this.onDelete}
          />

          {newEvent && (
            <label className="event-modal__checkbox-wrap">
              <input
                type="checkbox"
                className="event-modal__checkbox"
                value={isRecurMode}
                onChange={this.toggleCheckbox}
              />
              Recurring mode
            </label>
          )}

          {isRecurMode ? (
            <div className="recurring-block">
              <label>
                <Select
                  className="recurring-block__week-interval"
                  value={rrule.interval}
                  onChange={this.handleIntervalOnChange}
                  options={intervalOtpions}
                  defaultValue={intervalOtpions[0]}
                />
                Weeks interval
              </label>

              <label>
                <DatePicker
                  className="recurring-block__date-picker"
                  onChange={this.handleStartOnChange}
                  selected={rrule.dtstart}
                />
                Start interval
              </label>

              <label>
                <DatePicker
                  className="recurring-block__date-picker"
                  onChange={this.handleEndOnChange}
                  selected={rrule.until}
                />
                Finish interval
              </label>

              <label>
                <Select
                  className="recurring-block__week-select"
                  value={rrule.byweekday}
                  onChange={this.handleSelectOnChange}
                  options={options}
                  isMulti
                  defaultValue={options[0]}
                />
              </label>
            </div>
          ) : null}
        </Modal>
      </div>
    );
  }
}

export default hot(module)(App);
