import axios from 'axios';

const updateEventDate = (id, momentStart, momentEnd, startDay) => {
  return axios.put(`/api/events/${id}`, {
    start: momentStart.toDate(),
    end: momentEnd.toDate(),
    startDay,
  });
};

const updateEventTitle = (id, title) =>
  axios.put(`/api/events/${id}`, { title });

const deleteEvent = id => axios.delete(`/api/events/${id}`);

const fetchAllEvents = () => axios.get('/api/events');

const addNewEvent = (newEvent, title, rrule) =>
  axios.post('/api/events', { ...newEvent, title, rrule });

export default {
  updateEventDate,
  deleteEvent,
  fetchAllEvents,
  addNewEvent,
  updateEventTitle,
};
